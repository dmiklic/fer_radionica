#!/bin/bash
#SBATCH --job-name=omp_test
#SBATCH --time=9-00:10:00
#SBATCH --ntasks=1
#SBATCH -o omp-%j.out
#SBATCH --partition=comp_smp

#export OMP_NUM_THREADS=$SLURM_NTASKS

./Jacobi
