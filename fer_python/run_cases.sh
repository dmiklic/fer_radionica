#! /bin/bash

for i in {1..10}
do
	mkdir test_folder_$i
	cp slurm_script.sh test_folder_$i/
	cp *.py test_folder_$i/
	cd test_folder_$i
	eval "sbatch slurm_script.sh"
	echo "case $i started ..."
	cd ..
done
