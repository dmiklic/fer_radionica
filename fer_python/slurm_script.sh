#!/bin/bash
#SBATCH --job-name=py_job
#SBATCH --time=9-00:10:00
#SBATCH --ntasks=1
#SBATCH -o ncpus-%j.out
#SBATCH --partition=computes_thin

python3 python_testcase.py
