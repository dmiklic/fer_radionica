#!/bin/bash
#SBATCH --job-name=mpi_test
#SBATCH --time=999:10:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH -o logit-%j.out
#SBATCH --partition=computes_thin

#module load mpi/openmpi-x86_64
#source /opt/OpenFOAM/OpenFOAM-7/etc/bashrc

solver=simpleFoam
cores=4

blockMesh

decomposePar

mpirun -np $cores $solver -parallel > log.LOGGIT 2>&1
